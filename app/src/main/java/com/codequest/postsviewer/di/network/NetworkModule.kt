package com.codequest.postsviewer.di.network

import com.codequest.postsviewer.data.DuckRepository
import com.codequest.postsviewer.domain.GetDucksListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://random-d.uk/api/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideDuckService(retrofit: Retrofit) = retrofit.create(DucksService::class.java)

    @Provides
    fun provideIODispatcher() = Dispatchers.IO

    @Provides
    fun provideDuckRepository(ducksService: DucksService, dispatcher: CoroutineDispatcher) =
        DuckRepository(ducksService, dispatcher)

    @Provides
    fun provideGetDucksUseCase(repository: DuckRepository) = GetDucksListUseCase(repository)

}