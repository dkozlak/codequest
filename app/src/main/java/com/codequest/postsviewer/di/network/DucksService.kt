package com.codequest.postsviewer.di.network

import com.codequest.postsviewer.data.DuckDto
import retrofit2.http.GET

interface DucksService {

    @GET("random")
    suspend fun getDuck(): DuckDto
}