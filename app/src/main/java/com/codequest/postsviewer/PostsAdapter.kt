package com.codequest.postsviewer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.codequest.postsviewer.data.DuckDto
import com.codequest.postsviewer.databinding.ViewPostBinding

class PostsAdapter : RecyclerView.Adapter<PostsAdapter.PostViewHolder>() {

    private val list = mutableListOf<DuckDto>()

    fun updateData(newList: List<DuckDto>) {
        list.let {
            it.clear()
            it.addAll(newList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder(ViewPostBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class PostViewHolder(val binding: ViewPostBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(duckDto: DuckDto) {
            binding.duckImg.load(duckDto.url)
        }
    }
}