package com.codequest.postsviewer.data

import com.codequest.postsviewer.di.network.DucksService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DuckRepository @Inject constructor(
    private val ducksService: DucksService,
    private val dispatcher: CoroutineDispatcher
) {

    suspend fun getDuck() =
        withContext(dispatcher) {
            ducksService.getDuck()
        }

}