package com.codequest.postsviewer.data

import com.google.gson.annotations.SerializedName

data class DuckDto(
    @SerializedName("url") val url: String
)