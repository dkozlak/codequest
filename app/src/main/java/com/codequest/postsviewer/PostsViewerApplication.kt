package com.codequest.postsviewer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PostsViewerApplication: Application()