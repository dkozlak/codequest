package com.codequest.postsviewer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.codequest.postsviewer.data.DuckDto
import com.codequest.postsviewer.domain.GetDucksListUseCase
import com.codequest.postsviewer.domain.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val useCase: GetDucksListUseCase) : ViewModel() {

    private val _viewState = MutableLiveData<ViewState>()
    val viewState = _viewState as LiveData<ViewState>

    fun refresh() {
        viewModelScope.launch {
            val result = useCase.getDucks()
            when (result) {
                is Result.Success -> _viewState.value = ViewState.Success(result.value)
                is Result.GenericError -> _viewState.value = ViewState.Error(result.error)
            }
        }
    }
}

sealed class ViewState {

    class Success(val list: List<DuckDto>): ViewState()
    class Error(val exception: Exception): ViewState()
}