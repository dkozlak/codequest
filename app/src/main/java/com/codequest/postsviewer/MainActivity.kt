package com.codequest.postsviewer

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.codequest.postsviewer.data.DuckDto
import com.codequest.postsviewer.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    private val adapter = PostsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        setupViews()
        viewModel.viewState.observe(this, {
            when (it) {
                is ViewState.Success -> showList(it.list)
                is ViewState.Error -> showError(it.exception)
            }
        })
        viewModel.refresh()
    }

    private fun setupViews() {
        _binding.recyclerView.adapter = adapter
        _binding.refreshBtn.setOnClickListener { viewModel.refresh() }
    }

    private fun showList(list: List<DuckDto>){
        adapter.updateData(list)
    }

    private fun showError(exception: Exception) {
        Toast.makeText(baseContext, exception.toString(), Toast.LENGTH_LONG).show()
    }
}
