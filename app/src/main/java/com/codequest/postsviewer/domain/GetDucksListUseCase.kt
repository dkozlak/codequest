package com.codequest.postsviewer.domain

import com.codequest.postsviewer.data.DuckDto
import com.codequest.postsviewer.data.DuckRepository
import javax.inject.Inject

private const val DUCKS_TO_GET = 10

class GetDucksListUseCase @Inject constructor(private val repository: DuckRepository) {

    suspend fun getDucks() =
        try {
            val mutableList = mutableListOf<DuckDto>()
            for (i in 0..DUCKS_TO_GET) {
                mutableList.add(repository.getDuck())
            }
            Result.Success(mutableList)
        } catch (exception: Exception) {
            Result.GenericError(exception)
        }


}

sealed class Result<out T> {
    data class Success<out T>(val value: T) : Result<T>()
    data class GenericError(val error: Exception) : Result<Nothing>()
}